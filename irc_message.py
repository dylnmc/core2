# -*- coding: utf-8 -*-

from re import match as rematch

class IrcMessage(object):

    """IRC message"""

    # should be overwritten with IrcMessage._bot = .. before init
    """irc bot [core2.core2]"""
    _bot = None

    def __init__(self, raw, server_name):
        """init irc message

        :raw: raw irc message

        """
        splraw = raw.split()

        self._raw = raw
        self._server = self
        self._server_name = server_name
        self._command = splraw[1]
        self._prefix = splraw[0]

        # split prefix from nick[!user][@host] into nick,user,host
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if self._prefix.startswith(':'):
            self._prefix = self._prefix[1:]
            if '!' in self._prefix:
                if '@' in self._prefix:
                    self._nick = self._prefix.split('!', 1)
                    self._user = self._nick[1].split('@', 1)
                    self._host = self._user[1]
                    self._user = self._user[0]
                    self._nick = self._nick[0]
                else:
                    self._nick = self._prefix.split('!')
                    self._user = self._nick[1]
                    self._nick = self._nick[0]
                    self._host = None
            elif '@' in self._prefix:
                self._nick = self._prefix.split('!')
                self._host = self._nick[1]
                self._nick = self._nick[0]
                self._user = None
            else:
                self._nick = self._prefix
                self._user = None
                self._host = None
        else:
            # first word was not a prefix (e.g. PING,PONG)
            self._command = splraw[0]
            self._prefix = None

        # get middle and message
        # ~~~~~~~~~~~~~~~~~~~~~~
        self._message = None
        self._middle = []
        for ind,word in enumerate(splraw[1:] if self._prefix is None else splraw[2:]):
            if word.startswith(':'):
                ind += 1 if self._prefix is None else 2
                self._message = raw.split(None, ind)[ind][1:]
                break
            self._middle.append(word)

        # TODO: extras
        self.regex_groups = None

    def is_owner_nick(self):
        """checks if sender's nick matches bot's owner nick

        :returns: [bool]

        """
        return rematch(self.get_server()._owner_nick, self._nick)

    def is_owner_user(self):
        """checks if sender's user matches bot's owner user

        :returns: [bool]

        """
        return self._user is not None and rematch(self.get_server()._owner_user, self._user)

    def is_owner_host(self):
        """checks if sender's host matches bot's owner host

        :returns: [bool]

        """
        return self._host is not None and rematch(self.get_server()._owner_host, self._host)

    def get_server(self):
        """returns the server from which the message originated

        :returns: server [server.Server]

        """
        return self._bot._servers[self._server_name]

    def get_channel(self):
        if self._command in ('PRIVMSG', 'NOTICE', 'JOIN'):
            return self._middle[0]
        return None

    def say_back(self, msg):
        chan = self.get_channel()
        if chan is not None:
            self.get_server().say(chan, msg)
            return True
        return False

    def me_back(self, msg):
        chan = self.get_channel()
        if chan is not None:
            self.get_server().me(chan, msg)
            return True
        return False

    def __repr__(self):
        return self._raw

    def __str__(self):
        return '{} {} {} {}'.format(self._prefix, self._command, self._middle, self._message)

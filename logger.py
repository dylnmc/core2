#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from re import compile as recompile, sub as resub
from time import strftime, time

class Logger():

    """Easy-to-use file/stdout logger"""

    def __init__(self, **kwargs):
        """ init for Logger

            :**kwargs:
                :out_file: str - the file to log to
                :to_stdout: bool - indicates if should write to stdout
                :to_file: bool - indicates if should write to the file
                :append_file: bool - indicates if should append to end of file (otherwise overwritten)
                :header: str - what to print when first start logging ("%time%" replaced with current date/time)
                :time_format: str - the strftime representation of how the time should be represented
                :text_fmt: dict - used for prepending pretext as well as printing to stdout
        """

        # *list of color options*
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self._color_opts = {
            'fg': {
                'black':    '30;',
                'red':      '31;',
                'green':    '32;',
                'yellow':   '33;',
                'blue':     '34;',
                'magenta':  '35;',
                'cyan':     '36;',
                'white':    '37;',
                '256':      '38;05;{};',
                'default':  '39;',
            },
            'bg': {

                'black':    '40;',
                'red':      '41;',
                'green':    '42;',
                'yellow':   '43;',
                'blue':     '44;',
                'magenta':  '45;',
                'cyan':     '46;',
                'white':    '47;',
                '256':      '48;05;{};',
                'default':  '49;',
            },
        }
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        # *list of attribute options*
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self._attr_opts = {
            'clear':        '0;',
            'bold':         '1;',
            'faint':        '2;', # NOTE: hardly ever supported
            'italic':       '3;', # NOTE: not widely supported
            'underline':    '4;',
            'blink':        '5;', # NOTE: not widely supported
            'blink_fast':   '6;', # NOTE: hardly ever supported (sometimes same as blink)
            'reverse':      '7;',
            'conceal':      '8;', # NOTE: not widely supported
            'crossed_out':  '9;', # NOTE: basically never works
        }
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        # *text format before and after - dict { str name/type : tuple ( str "pre-text", str "post-text" ) }*
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.text_fmt = {
            'normal':       (   '== ',
                                ' ==',
                            ),

            'good':         (   '{}++{} '.format(self.get_color(fg=15, bg='green'), self.get_color(fg='green', bg='default')),
                                ' {}++'.format(self.get_color(fg=15, bg='green')),
                            ),

            'bad':          (   '{}--{} '.format(self.get_color(fg=15, bg='red'), self.get_color(fg='red', bg='default')),
                                ' {}--'.format(self.get_color(fg=15, bg='red')),
                            ),

            'warning':      (   '{}~~{} '.format(self.get_color(fg=15, bg='yellow'), self.get_color(fg='yellow', bg='default')), 
                                ' {}~~'.format(self.get_color(fg=15, bg='yellow')),
                            ),

            'error':        (   '{}**{} '.format(self.get_color('underline', fg=15, bg='red'), self.get_color(fg='red', bg='default')), 
                                ' {}**'.format(self.get_color(fg=15, bg='red')),
                            ),

            'important':    (   '{}!! '.format(self.get_color('bold', fg=15)),
                                ' !!',
                            ),

            'hide':         (   '{}`` '.format(self.get_color('conceal')), # NOTE: sometimes CONCEAL doesn't work (like in urxvt)
                                ' ``',
                            ),

            'trivial':      (   '{}// '.format(self.get_color('italic')), # NOTE: ITALIC is likely to not work
                                ' //',
                            ),
        }
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        # *create self.text_fmt_file (removed ansi escape sequences)*
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # text format before and after for files (not ansi escape sequences)
        self.text_fmt_file = {}

        # used for stripping ansi before inserting into file
        ansi_escape = recompile(r'\x1b[^m]*m')
        for key, val in list(self.text_fmt.items()):
            if type(key) not in (list, tuple) and len(val) != 2:
                del self.text_fmt[key]
                continue
            self.text_fmt_file[key] = (resub(ansi_escape, '', val[0]), resub(ansi_escape, '', val[1]))
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        # *other opts*
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # indicates if should write to stdout
        self.to_stdout = bool(kwargs.get('to_stdout', True))

        self.to_file = bool(kwargs.get('to_file', True))

        # the file to log to
        self.out_file = str(kwargs.get('out_file', './logfile_{}.log'.format(time())))

        # indicates if should append to end of file (otherwise overwritten)
        self.append_file = bool(kwargs.get('append_file', True))

        # what to print to log file when first start logging ("%time%" replaced with current date/time
        self.header = kwargs.get('header', '-- %time% --')

        # the strftime representation of how the time should be represented
        self.time_format = str(kwargs.get('time_format', '%a, %b %d, %Y, %H.%M.%S.%f'))

        # file that gets opened
        self.f = open(self.out_file, 'a' if self.append_file else 'w')

        # write header to file
        if self.to_file:
            self.f.write('{}\n'.format(self.header.replace('%time%', strftime(self.time_format.replace('%f', str(time()).split('.')[1])))))

    def get_color(self, *attrs, **kwargs):
        """ gets the ansi fg/bg colors or attributes for text

        :*attrs: attrs from self._attr_opts
        :**kwargs:
            :fg: fg color from self._color_opts['fg']
            :bg: bg color from self._color_opts['bg']
        :returns: str - the ansi escape sequence

        """
        # ansi escape sequence
        color = '\x1b['

        # attributes used from *attrs
        used_attrs = []
        if 'clear' in attrs:
            # should put this first
            used_attrs.append('clear')
            color += self._attr_opts['clear']

        # fg color (see self._color_opts['fg'])
        fg = kwargs.get('fg', None)
        # bg color (see self._color_opts['bg'])
        bg = kwargs.get('bg', None)

        # add fg color if applicaple
        if fg in self._color_opts['fg']:
            color += self._color_opts['fg'][fg]
        elif type(fg) is int and fg >= 0 and fg < 256:
            color += self._color_opts['fg']['256'].format(fg)

        # add bg color if applicaple
        if bg in self._color_opts['bg']:
            color += self._color_opts['bg'][bg]
        elif type(bg) is int and bg >= 0 and bg < 256:
            color += self._color_opts['bg']['256'].format(bg)

        for attr in attrs:
            if attr in self._attr_opts:
                if attr in used_attrs:
                    continue
                color += self._attr_opts[attr]
                used_attrs.append(attr)

        if len(color) == 2:
            return ''
        else:
            return '{}m'.format(color[:-1])


    def log(self, line, typ=None, **kwargs):
        """ log to file and/or print to stdout line

        :line: the line to be written/printed
        :typ: the type of line (normal, error, etc) (see self.text_fmt) (default: None)
        :**kwargs:
                :to_file: should write to file?
                :to_stdout: should print to stdout?
        :returns: None

        """
        to_file = bool(kwargs.get('to_file', self.to_file))
        to_stdout = bool(kwargs.get('to_stdout', True))

        if not typ:
            typ = 'normal'

        if to_file:
            self.f.write('{}{}{}\n'.format(self.text_fmt_file[typ][0], line, self.text_fmt_file[typ][1]))
            self.f.flush()

        if to_stdout:
            print('{}{}{}'.format(self.text_fmt[typ][0], line, self.text_fmt[typ][1]), end='\x1b[0m\n')

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from argparse import ArgumentParser
from codecs import encode as cencode
from getpass import getpass
from inspect import stack
from json import dump as jdump, load as jload
from json.decoder import JSONDecodeError
from os import chdir, mkdir
from os.path import abspath, dirname, expanduser, expandvars, isdir, isfile
from queue import Queue
from shutil import copyfile as cp
from threading import Thread

from event_handler import EventHandler
from irc_message import IrcMessage
from logger import Logger
from server import Server

class core2(object):

    """simple ssl, multi-server irc bot"""

    _version = '1.3.0'

    def __init__(self, args=()):
        """initialize core2 """

        chdir(dirname(stack()[0][1]))

        # create logger
        self._logger = Logger(out_file='log')

        # set loggers
        Server._logger = self._logger
        EventHandler._logger = self._logger

        # set rererence to bot in irc message
        IrcMessage._bot = self

        # parse args (should be first after creating loggers)
        self._parse_args_(args)

        # parse configs
        self._servers = {}
        self._parse_configs_()

        self._event_handler = EventHandler(self._module_dirs)

    def start(self):
        """initiate and start all servers

        :returns: None

        """

        self._queue = Queue()

        # start servers
        for server_name, server in self._servers.items():
            server['name'] = server_name
            self._start_server_(server)

    def mainloop(self):
        """mainloop for core2

        :returns: None

        """
        try:
            while len(self._servers):
                self._event_handler.process(self._queue.get())
        except KeyboardInterrupt:
            print('terminating')

    def _start_server_(self, server):
        """start a server

        :server: server variable as a dict
        :returns: None

        """
        server = Server(**server)
        self._servers[server._name] = server
        Thread(target=self._start_server_wrapper_, args=(server,)).start()

    def _start_server_wrapper_(self, server):
        """wrapper for attempting to connect to server with thread

        :server: the server to connect to
        :returns: None

        """
        try:
            server.connect()
            server.mainloop(self._queue)
        except RuntimeError as e:
            self._logger.log(e, 'error')

    def _parse_configs_(self):
        """parses configs inside self._config_dir

        :returns: None

        """
        default_config = {
            'servers': {
                'freenode': {
                    'nickserv_nick': 'core2_duo',
                    'nick': 'core2_duo',
                    'realname': 'core2 duo',
                    'password_file': '.freenode_password',
                    'host': 'chat.freenode.net',
                    'port': 6697,
                    'ssl': True,
                    'channels': ['##core2'],
                    'onjoin': {'##core2': '/me stumbles back in'}
                }
            }
        }

        if not isdir(self._config_dir):
            self._logger.log('config dif, {}, not yet created - creating', 'warning')
            mkdir(self._config_dir)
        if not isfile('{}/config.json'.format(self._config_dir)):
            with open('{}/config.json'.format(self._config_dir), 'w') as f:
                jdump(default_config, f, indent=2)
            config = default_config
        else:
            try:
                with open('{}/config.json'.format(self._config_dir)) as f:
                    config = jload(f)
            except JSONDecodeError as e:
                self._logger.log('could not properly load config file, {}/config.json'.format(self._config_dir), 'warning')
                self._logger.log('copying to config_broken.old and replacing with default config')
                cp('{}/config'.format(self._config_dir), '{}/config_broken.old'.format(self._config_dir))
                with open('{}/config.json'.format(self._config_dir), 'w') as f:
                    jdump(f, default_config)
                config = default_config

        globs = config.get('globals', {})
        self._servers = config.get('servers', {})

        for server_name,server in self._servers.items():
            password_file = abspath(expanduser(expandvars('{}/{}'.format(self._config_dir, server['password_file']))))
            if not isfile(password_file):
                try:
                    while True:
                        _p = getpass('enter password for {} (^c to skip): '.format(server_name))
                        if _p != getpass('confirm password: '):
                            print('Password mismatch. Try again.')
                            continue
                        break
                    with open(password_file, 'wb') as f:
                        f.write(cencode(cencode(_p, 'rot13').encode('ascii'), 'base64'))
                except KeyboardInterrupt:
                    print('\nskipped')
            server['password_file'] = password_file

        # globals
        self._module_dirs = globs.get('module_dirs', ['./modules'])

    def _parse_args_(self, args):
        """parses sys.argv args (or other args)

        :args: (argv?) args
        :returns: None

        """
        # if len(args) != 0 and args[0] == __file__:
        #     args = args[1:]
        args = args[1:]

        parser = ArgumentParser(prog = './core2', description = 'a small python3 bot that connects to an irc network', epilog = 'have fun ^u^')
        parser.add_argument('-v', '--verbosity', help = 'increase output verbosity', action = 'count', default = 0)
        parser.add_argument('-V', '--version', help = 'prints the version', action = 'store_true', default = False)
        parser.add_argument('-c', '--config', help = 'specifies config directory. Default is ~/.core2/', action = 'store', default = '~/.core2/')

        args = parser.parse_args(args)

        if args.version:
            print('version: {}'.format(core2.__version__))
            exit(0)

        self._config_dir = abspath(expanduser(expandvars(args.config)))
        self._verbosity = args.verbosity


def main():
    from sys import argv

    bot = core2(argv)
    bot.start()
    bot.mainloop()

if __name__ == "__main__":
    main()

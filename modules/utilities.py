
from re import compile as recompile, match as rematch
from subprocess import run, PIPE
from tempfile import NamedTemporaryFile

def __setup__():
    global re_empty_line
    re_empty_line = recompile('^\s*$')

def raw(msg):
    if not msg.is_owner_host():
        return
    msg.get_server().raw(msg._message.split(None, 1)[1])

raw.message_command = '!raw'
raw.command_equals = 'PRIVMSG'


def join(msg):
    if not msg.is_owner_host():
        return
    msg.get_server().join(msg._message.split(None, 1)[1])

join.message_command = '!join'
join.command_equals = 'PRIVMSG'


def say(msg):
    if not msg.is_owner_host():
        return
    channels = msg._message.split(None, 2)
    message = channels[2]
    channels = channels[1]
    msg.get_server().say(channels, message)

say.message_command = '!say'
say.command_equals = 'PRIVMSG'


def me(msg):
    if not msg.is_owner_host():
        return
    channels = msg._message.split(None, 2)
    message = channels[2]
    channels = channels[1]
    msg.get_server().me(channels, message)

me.message_command = '!me'
me.command_equals = 'PRIVMSG'


def notice(msg):
    if not msg.is_owner_host():
        return
    channels = msg._message.split(None, 2)
    message = channels[2]
    channels = channels[1]
    msg.get_server().notice(channels, message)

notice.message_command = '!notice'
notice.command_equals = 'PRIVMSG'


def mode(msg):
    if not msg.is_owner_host():
        return
    mode = msg._message.split()
    if len(mode) < 3:
        return
    elif len(mode) == 3:
        if not mode[1][0] in ('+', '-'):
            return
        channel = msg.get_channel()
        idents = mode[2]
        mode = mode[1]
    else:
        if mode[1][0] in ('#', '&'):
            if not mode[2][0] in ('+', '-'):
                return
            channel = mode[1]
            idents = ' '.join(mode[3:])
            mode = mode[2]
        else:
            if not mode[1][0] in ('+', '-'):
                return
            channel = msg.get_channel()
            idents = ' '.join(mode[2:])
            mode = mode[1]

    msg.get_server().mode(channel, mode, idents)

mode.message_command = '!mode'
mode.command_equals = 'PRIVMSG'


def kick(msg):
    if not msg.is_owner_host():
        return
    ident = msg._message.split()

    if ident[1][0] in ('#', '&'):
        if len(ident) < 3:
            return

        channel = ident[1]

        if len(ident) > 3:
            message = ' '.join(ident[3:])
        else:
            message = None

        ident = ident[2]
    else:
        if len(ident) < 2:
            return

        channel = msg.get_channel()

        if len(ident) > 2:
            message = ' '.join(ident[2:])
        else:
            message = None

        ident = ident[1]

    if message:
        msg.get_server().raw('KICK {} {} :{}'.format(channel, ident, message))
    else:
        msg.get_server().raw('KICK {} {}'.format(channel, ident))

kick.message_command = '!kick'
kick.command_equals = 'PRIVMSG'


def op(msg):
    if not msg.is_owner_host():
        return
    splmsg = msg._message.split()[1:]

    if len(splmsg) > 0 and splmsg[0][0] in ('#', '&'):
        chan = splmsg[0]
        splmsg = splmsg[1:]
    else:
        chan = msg.get_channel()

    if len(splmsg) == 0:
        users = [msg._nick]
    else:
        users = set(splmsg)

    msg.get_server().mode(chan, '+{}'.format('o' * len(users)), ' '.join(users))

op.message_command = '!op'
op.command_equals = 'PRIVMSG'


def deop(msg):
    if not msg.is_owner_host():
        return
    splmsg = msg._message.split()[1:]

    if len(splmsg) > 0 and splmsg[0][0] in ('#', '&'):
        chan = splmsg[0]
        splmsg = splmsg[1:]
    else:
        chan = msg.get_channel()

    if len(splmsg) == 0:
        users = [msg._nick]
    else:
        users = set(splmsg)

    msg.get_server().mode(chan, '-{}'.format('o' * len(users)), ' '.join(users))

deop.message_command = '!deop'
deop.command_equals = 'PRIVMSG'


def chanop(msg):
    if not msg.is_owner_host():
        return
    splmsg = msg._message.split()[1:]

    if len(splmsg) > 0 and splmsg[0][0] in ('#', '&'):
        chan = splmsg[0]
        splmsg = splmsg[1:]
    else:
        chan = msg.get_channel()

    if len(splmsg) == 0:
        splmsg.append(msg._nick)

    msg.get_server().say('chanserv', 'op {} {}'.format(chan, ' '.join(splmsg)))

chanop.message_command = '!chanop'
chanop.command_equals = 'PRIVMSG'


def chandeop(msg):
    if not msg.is_owner_host():
        return
    splmsg = msg._message.split()[1:]

    if len(splmsg) > 0 and splmsg[0][0] in ('#', '&'):
        chan = splmsg[0]
        splmsg = splmsg[1:]
    else:
        chan = msg.get_channel()

    if len(splmsg) == 0:
        splmsg.append(msg._nick)

    msg.get_server().say('chanserv', 'deop {} {}'.format(chan, ' '.join(splmsg)))

chandeop.message_command = '!chandeop'
chandeop.command_equals = 'PRIVMSG'


def voice(msg):
    if not msg.is_owner_host():
        return
    splmsg = msg._message.split()[1:]

    if len(splmsg) > 0 and splmsg[0][0] in ('#', '&'):
        chan = splmsg[0]
        splmsg = splmsg[1:]
    else:
        chan = msg.get_channel()

    if len(splmsg) == 0:
        users = [msg._nick]
    else:
        users = set(splmsg)

    msg.get_server().mode(chan, '+{}'.format('v' * len(users)), ' '.join(users))

voice.message_command = '!voice'
voice.command_equals = 'PRIVMSG'


def devoice(msg):
    if not msg.is_owner_host():
        return
    splmsg = msg._message.split()[1:]

    if len(splmsg) > 0 and splmsg[0][0] in ('#', '&'):
        chan = splmsg[0]
        splmsg = splmsg[1:]
    else:
        chan = msg.get_channel()

    if len(splmsg) == 0:
        users = [msg._nick]
    else:
        users = set(splmsg)

    msg.get_server().mode(chan, '-{}'.format('v' * len(users)), ' '.join(users))

devoice.message_command = '!devoice'
devoice.command_equals = 'PRIVMSG'


def chanvoice(msg):
    if not msg.is_owner_host():
        return
    splmsg = msg._message.split()[1:]

    if len(splmsg) > 0 and splmsg[0][0] in ('#', '&'):
        chan = splmsg[0]
        splmsg = splmsg[1:]
    else:
        chan = msg.get_channel()

    if len(splmsg) == 0:
        splmsg.append(msg._nick)

    msg.get_server().say('chanserv', 'voice {} {}'.format(chan, ' '.join(splmsg)))

chanvoice.message_command = '!chanvoice'
chanvoice.command_equals = 'PRIVMSG'


def chandevoice(msg):
    if not msg.is_owner_host():
        return
    splmsg = msg._message.split()[1:]

    if len(splmsg) > 0 and splmsg[0][0] in ('#', '&'):
        chan = splmsg[0]
        splmsg = splmsg[1:]
    else:
        chan = msg.get_channel()

    if len(splmsg) == 0:
        splmsg.append(msg._nick)

    msg.get_server().say('chanserv', 'devoice {} {}'.format(chan, ' '.join(splmsg)))

chandevoice.message_command = '!chandevoice'
chandevoice.command_equals = 'PRIVMSG'


def reload(msg):
    if not msg.is_owner_host():
        return
    errors = msg._bot._event_handler.load_hooks()
    msg.say_back('reloaded hooks with {} error{}'.format(len(errors), 's' if len(errors) != 1 else ''))

reload.message_command = '!reload'
reload.command_equals = 'PRIVMSG'


def chan_flags(msg):
    if not msg.is_owner_host():
        return

    splmsg = msg._message.split()[1:]

    if len(splmsg) > 0 and splmsg[0][0] in ('#', '&'):
        chan = splmsg.pop(0)
    else:
        chan = msg.get_channel()

    if len(splmsg) >= 2:
        user = splmsg[0]
        flags = splmsg[1]
        splmsg = splmsg[2:]
    elif len(splmsg) == 1:
        user = msg._nick
        flags = splmsg[0]
        splmsg = []
    else:
        msg.say_back('syntax: !chanflags [#channel] [user] flags')
        return

    if flags[0] not in ('-', '+'):
        msg.say_back('Warning: "' + flags + '" does not look like a valid flag')

    m = 'flags {} {} {}'.format(chan, user, flags)
    if splmsg:
        m += ' ' + ' '.join(splmsg)
    msg.say_back('[> chanserv] ' + m)
    msg.get_server().say('chanserv', m)

chan_flags.message_command = '!chanflags'
chan_flags.command_equals = 'PRIVMSG'


def bot_eval(msg):
    if not msg.is_owner_host():
        return

    try:
        try:
            ret = eval(msg._message.split(' ', 1)[1])
        except:
            ret = exec(msg._message.split(' ', 1)[1])
    except Exception as e:
        ret = 'error: {}'.format(e)
    finally:
        msg.say_back('ret: {}'.format(repr(ret)))

bot_eval.message_command = '!utileval'
bot_eval.command_equals = 'PRIVMSG'


def shell(msg):
    if not msg.is_owner_host():
        return

    global re_empty_line

    p = run(['sh', '-c', msg._message.split(None, 1)[1]], stdout=PIPE, stderr=PIPE)

    out = p.stdout
    if out is not None:
        out = out.decode().rstrip().split('\n')
        if len(out) < 3:
            for line in out:
                if rematch(re_empty_line, line):
                    continue
                msg.say_back(line)
        else:
            f = NamedTemporaryFile(mode='w', delete=False)
            for line in out:
                f.write(line + '\n')
            f.close()
            tmp_p = run(['curl', '-Nsn', '-F', "f:1=@" + f.name, 'ix.io'], stdout=PIPE, stderr=PIPE)
            if tmp_p.stderr:
                msg.say_back('Error uploading to ix.io: \x0304' + tmp_p.stderr.decode().rstrip())
            else:
                if tmp_p.stdout:
                    msg.say_back(tmp_p.stdout.decode().rstrip())
                else:
                    msg.say_back('No output from ix.io?')
            # msg.say_back(' .. '.join(p.stdout.rstrip().split('\n')))


    msg.say_back
    err = p.stderr
    if err is not None:
        err = err.decode().rstrip().split('\n')
        for line in err:
            if rematch(re_empty_line, line):
                continue
            msg.say_back('\x0304' + line)

    if p.returncode != 0:
        msg.me_back(p.returncode)

shell.message_command = '$'
shell.command_equals = 'PRIVMSG'


# -*- coding: utf-8 -*-

from json import dump as jdump, load as jload
from os import walk
from os.path import isfile
from re import compile as recompile, match as rematch
from subprocess import getoutput

from modules.notify import Notify


def __setup__():
    global tell_list, re_empty_line
    if not isfile('.tells.json'):
        with open('.tells.json', 'w') as f:
            tell_list = []
            jdump(tell_list, f)
    else:
        with open('.tells.json') as f:
            tell_list = jload(f)

    re_empty_line = recompile('^\s*(#.*)?#')


def notify_in(msg):
    m = msg._message

    to = m.split('tell')[1].split(None, 1)
    remind_msg = to[1]
    to = to[0]

    tmp = m.lower().split()
    times = dict()
    for i in range(1, tmp.index('tell'), 2):
        key = tmp[1 + 1]
        if key[-1] != 's':
            key += 's'
        times[key] = tmp[i]

    secs = float(times.pop('seconds', 0))

    Notify.wait_for(secs - 1 if secs > 1 else secs, msg.get_server().say, msg.get_channel(), '{}: {}'.format(to, remind_msg), **times)

notify_in.message_regex = r'\.?in\s+(.*?)\s+(?=tell)tell\s+(\S+)\s([^\n]+)'
notify_in.command_equals = 'PRIVMSG'


def tell(msg):
    global tell_list

    m = msg._message

    to = m.split()[1]
    message = m.split(None, 2)[2]

    # keep these as empty strings (bc split might make them empty anyway)
    user = ''
    host = ''

    if m.count('!') == 0 and m.count('@') == 0:
        nick = to
    elif m.count('!') == 1 and m.count('@') == 0:
        nick = to.split('!')
        user = nick[1]
        nick = nick[0]
        host = ''
    elif m.count('!') == 0 and m.count('@') == 1:
        nick = to.split('@')
        user = ''
        host = nick[1]
        nick = nick[0]
    elif m.count('!') == 1 and m.count('@') == 1:
        nick = to.split('!')
        user = nick[1].split('@')
        host = user[1]
        user = user[0]
        nick = nick[0]
    else:
        msg.say_back('Invalid ident {}'.format(to))

    if not nick or nick == '*':
        nick = r'[^!]+'
    if not user or user == '*':
        user = r'[^@]+'
    if not host or host == '*':
        host = r'[^\n]+'

    to = '{}!{}@{}'.format(nick, user, host)

    tell = [to, msg._nick, msg.get_channel() if msg.get_channel() != msg._nick else 'PRIVMSG', message, msg._server_name]
    tell_list.append(tell)
    with open('.tells.json') as f:
        tells = jload(f)
    tells.append(tell)
    with open('.tells.json', 'w') as f:
        jdump(tells, f)

    msg.say_back('{}: I\'ll tell {} next time I see him/her'.format(msg._nick, to.replace(r'[^!]+', '*').replace(r'[^@]+', '*').replace(r'[^\n]+', '*')))

tell.message_command = '.tell'
tell.command_equals = 'PRIVMSG'


def check_tell(msg):
    global tell_list

    if not len(tell_list):
        return

    msg_ident = '{}!{}@{}'.format(msg._nick, msg._user, msg._host)
    tell_list = tell_list
    told = False
    for ind in range(len(tell_list) - 1, -1, -1):
        ident, from_nick, from_channel, message, server_name = tell_list[ind]
        if rematch(ident, msg_ident):
            told = True
            msg.say_back('{}: {} in {} (on {}) said: {}'.format(msg._nick, from_nick, from_channel, server_name, message))
            del tell_list[ind]

    if told:
        with open('.tells.json', 'w') as f:
            jdump(tell_list, f)

check_tell.command_tuple = ('PRIVMSG', 'JOIN')


def tellme(msg):
    if not msg.is_owner_host():
        return
    msg.say_back(tell_list)

tellme.message_command = '.tellme'
tellme.command_equals = 'PRIVMSG'


def rmtell(msg):
    if not msg.is_owner_host():
        return
    tell_list = []
    with open('.tells.json', 'w') as f:
        jdump(tell_list, f)

rmtell.message_command = '.rmtell'
rmtell.command_equals = 'PRIVMSG'


def code(msg):
    global re_empty_line
    sloc = 0
    pyfs = 0
    for dirpath, dirnames, filenames in walk('.'):
        for filename in filenames:
            if not filename.endswith('.py'):
                continue
            cloc = 0
            with open('{}/{}'.format(dirpath, filename)) as f:
                for line in f:
                    if rematch(re_empty_line, line):
                        continue
                    cloc += 1
            sloc += cloc
            if cloc > 0:
                pyfs += 1
    msg.say_back("I am located at " + getoutput('git ls-remote --get-url origin').rstrip('.git') +
            " and I contain {} lines of code in {} python files".format(sloc, pyfs))

code.message_command = '.code'
code.command_equals = 'PRIVMSG'


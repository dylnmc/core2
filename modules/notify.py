#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__no_load__ = True # don't load in event_handlery.py as a hook

from threading import Thread
from time import sleep, time

class Notify():

    """Simple notify class to keep track of time and then notify"""

    _last_time_surplus = 2

    def wait_for(secs, cb, *args, **kwargs):
        secs += float(kwargs.get('minutes', 0)) * 60
        secs += float(kwargs.get('hours', 0)) * 3600
        secs += float(kwargs.get('days', 0)) * 86400
        if secs < 0:
            return False
        Thread(target=Notify._wait_until, args=(time()+secs, cb, *args)).start()
        return True

    def notify_at_dt(dt, cb, *args):
        """call callback function, cb, at datetime, dt

        :dt: datetime object
        :cb: callback function
        :returns: bool - True if time in future, False otherwise

        """
        dt_ts = dt.timestamp()
        if dt_ts < time():
            # dt timestamp in past
            return False
        Thread(target=Notify._wait_until, args=(dt_ts, cb, *args)).start()
        return True

    def _wait_until(until_ts, cb, *args):
        now = time()
        while until_ts > now + Notify._last_time_surplus:
            sleep((until_ts - now) * 2 / 3)
            now = time()
        if now < until_ts:
            sleep(until_ts - now)
        Thread(target=Notify._cb_thread_wrapper, args=(cb, *args)).start()

    def _cb_thread_wrapper(cb, *args):
        try:
            cb(*args)
        except Exception as e:
            print('error: {}'.format(e))


if __name__ == "__main__":
    from datetime import datetime, timedelta

    def cb(foo, bar):
        print('args: {{foo: {}, bar: {}}}'.format(foo, bar))

    # test notify_at_dt()
    now = datetime.now()
    now += timedelta(0, 5) # add 5 seconds
    Notify.notify_at_dt(now, cb, 'foooo', 'baaar')

    # test wait_for()
    Notify.wait_for(2, cb, 'FOOOO', 'BAAAR')

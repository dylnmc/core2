
# from bs4 import BeautifulSoup
from codecs import encode
from datetime import datetime
from json import dump as jdump,  load as jload, loads as jloads
from os.path import isfile
from pytz import timezone
from random import random, randint
from re import compile as recompile, match as rematch
from requests import get as rqget
from urllib.parse import quote, quote_plus, urlencode
from urllib.request import ProxyHandler, Request, urlopen, URLError

cities_weather = None
kirbies = None
inspiration_add_regex = r'"[^"]+" - [\w .]+'
inspiration = None
inspiration_use = None
cities = None
timezones = None

def __setup__():
    global kirbies, inspiration, inspiration_use, cities, timezones, cities_weather

    with open('.cities_weather.txt') as f:
        # id	nm	lat	lon	countryCode
        cities_weather = [line.rstrip().split('\t') for line in f.readlines()]

    inspiration = []
    with open('.inspirational_quotes') as f:
        for line in f:
            inspiration.append(line.strip())
    inspiration_use = inspiration.copy()

    with open('.kirbies.json') as f:
        kirbies = jload(f)

    # timezone
    cities = {}
    timezones = []
    i = 0
    with open('.cities.txt') as f:
        for line in f:
            line = line.split('\t')
            # [{ ascii_name: timezone }]
            city = line[2].lower()
            if city not in cities:
                cities[city] = []
            cities[city].append(i)
            timezones.append((line[-2], line[-9], line[-11]))
            i += 1


def weather(msg):
    splmsg = msg._message.split()[1:]
    if len(splmsg) > 0:
        loc = splmsg[0]
    else:
        return

    global cities_weather

    res = []
    for city in cities_weather:
        if city[1] == loc:
            res.append(city)

    if len(res) == 0:
        msg.say_back('no results')
        return

    if len(splmsg) > 1:
        loc += ',' + splmsg[1]
    r = jloads(urlopen('http://api.openweathermap.org/data/2.5/weather?q={}&APPID=26dd8032b21d206a2d9c7b267db6d826').read().decode('ascii', errors='ignore'))
    # msg.say_back('{}, {} <{},{}>: {{sunrise: }}')
    msg.say_back(str(r))

weather.message_command = '.weather'
weather.command_equals = 'PRIVMSG'

def fight(msg):
    msg.say_back(' '.join([str(int(''.join([str(ord(ch)) for ch in encode(encode(encode(w, 'rot13').encode('ascii'), 'bz2'), 'base64').decode()])[32:42])) for w in msg._message.split()[1:]]))

fight.message_command = '.fight'
fight.command_equals = 'PRIVMSG'


def kirby_say(msg):
    splmsg = msg._message.split()[1:]

    flag1 = None
    flag2 = None

    lenmsg = len(splmsg)

    if len(splmsg) > 0 and splmsg[0][0] == '-':
        flag1 = splmsg[0][1:]
        lenmsg -= 1
        if len(splmsg) > 1 and splmsg[1][0] == '-':
            flag2 = splmsg[1][1:]
            lenmsg -= 1

    if flag1 == 'help':
        msg.say_back('\x0307<(^_^)> ~ \x0309.kirbysay -list [-<kirby>] \x0302all kirbies or all variations or a type of kirby where <kirby> is a kirby in -list if second flag specified\x03  \x0309.kirbysay [-<kirby> [-<variation>]] [message] \x0302uses optional kirby and variation flags to print a kirby message \x0307~ <(^_^)>')
        return

    if flag1 == 'list':
        if flag2:
            if flag2 in kirbies:
                kirby = kirbies[flag2]
                kirbs = []
                for kirbvariation, kirb in kirby.items():
                    if kirbvariation == 'default':
                        continue
                    kirbs.append('{}: {}'.format(kirbvariation, kirb))
                msg.say_back('; '.join(kirbs))
            else:
                msg.say_back('I didn\'t find that kirby')
        else:
            msg.say_back('kirbies: -{}'.format('; -'.join(kirbies.keys())))
        return

    if lenmsg == 0:
        m = '...'
    elif flag2:
        m = msg._message.split(None, 3)[3]
    elif flag1:
        m = msg._message.split(None, 2)[2]
    else:
        m = msg._message.split(None, 1)[1]

    kirby = kirbies[flag1] if flag1 in kirbies else kirbies['normal']
    kirby = kirby[flag2] if flag2 in kirby else kirby['default']

    msg.say_back('{} ~ {}'.format(kirby, m))

kirby_say.message_command = '.kirbysay'
kirby_say.command_equals = 'PRIVMSG'


def insp(msg):
    global inspiration_use
    ll = len(inspiration_use)
    if ll == 0:
        global inspiration
        inspiration_use = inspiration
        ll = len(inspiration_use)
    msg.say_back(inspiration_use.pop(randint(0, ll - 1)))

insp.message_command = '.inspiration'
insp.command_equals = 'PRIVMSG'


def add_insp(msg):
    global inspiration_add_regex

    message = msg._message.split(None, 1)

    if len(message) == 1:
        msg.say_back('syntax: .addinsp "<quote>" - author')
        return

    quote = message[1].rstrip()
    double_count = quote.count('\"')

    if double_count < 2:
        msg.say_back('Please surround quotes with double quotes (\")')
        return
    if double_count > 2:
        msg.say_back('Please only use double quotes to surround the quotes. Change any double quotes within the quote to single quotes.')
        return
    if not rematch(inspiration_add_regex, quote):
        msg.say_back('Please make the quote resemble `"<quote>" - author` the quote may be anything, but the author\'s name should be only ascii characters, spaces and periods as needed')
        return

    if isfile('.inspirational_quotes_add.json'):
        with open('.inspirational_quotes_add.json') as f:
            insp = jload(f)
    else:
        insp = {}

    if quote in inspiration:
        msg.say_back('This quote is already being used ^_^')
        return

    if not msg._nick in insp:
        insp[msg._nick] = []

    if quote in insp[msg._nick]:
        msg.say_back('You have already added this quote. It is still awaiting the owner\'s approval. Fear not, if it is a useful quote, it will probably be added :D')
        return

    insp[msg._nick].append(quote)
    with open('.inspirational_quotes_add.json', 'w') as f:
        jdump(insp, f)

    msg.say_back('Awaiting approval from owner, but it has been added :)')

add_insp.message_command = '.addinsp'
add_insp.command_equals = 'PRIVMSG'


def show_added_insp(msg):
    if not isfile('.inspirational_quotes_add.json'):
        insp_add = {}
    else:
        with open('.inspirational_quotes_add.json') as f:
            insp_add = jload(f)

    if len(insp_add) == 0:
        msg.say_back('No quotes yet')
        return

    splmsg = msg._message.split()[1:]

    nick = None
    ind = None
    if len(splmsg) > 0:
        nick = splmsg[0]
        if len(splmsg) > 1:
            try:
                ind = int(splmsg[1])
            except ValueError:
                pass

    if nick is None:
        adders = []
        for nick, quotes in insp_add.items():
            adders.append('{} ({})'.format(nick, len(quotes)))
        msg.say_back('Quotes awaiting approval: {}'.format('; '.join(adders)))
    elif ind is None:
        if nick not in insp_add:
            msg.say_back('{} is not awaiting approval for any quotes'.format(nick))
            return
        if len(insp_add[nick]) > 3:
            msg.say_back('{} has added {} quotes awaiting approval ... showing last 3'.format(nick, len(insp_add[nick])))
            for i in range(-3, 0):
                msg.say_back(insp_add[nick][i])
        else:
            msg.say_back('{} is awaiting approval for {} quote{}'.format(nick, len(insp_add[nick]), 's' if len(insp_add[nick]) != 1 else ''))
            for i in range(len(insp_add[nick])):
                msg.say_back(insp_add[nick][i])
    else:
        if nick not in insp_add:
            msg.say_back('{} is not awaiting approval for any quotes'.format(nick))
            return
        try:
            msg.say_back(insp_add[nick][ind])
        except IndexError:
            msg.say_back('{} doesn\'t have quote at index {}'.format(nick, ind))

show_added_insp.message_command = '.showinsp'
show_added_insp.command_equals = 'PRIVMSG'


def approve_insp(msg):
    if not msg.is_owner_host():
        return

    splmsg = msg._message.split()[1:]

    nick = None
    ind = None
    if len(splmsg) > 0:
        nick = splmsg[0]
        if len(splmsg) > 1:
            try:
                ind = int(splmsg[1])
            except ValueError:
                pass

    if not isfile('.inspirational_quotes_add.json'):
        insp = {}
    else:
        with open('.inspirational_quotes_add.json') as f:
            insp = jload(f)

    global inspiration, inspiration_use

    if nick is None:
        msg.say_back('must specify a nick [and optional index] to approve')
        return

    if ind is None:
        if nick not in insp:
            msg.say_back('{} is not awaiting approval for any quotes'.format(nick))
            return
        msg.say_back('Approving {} quote{} from {}'.format(len(insp[nick]), 's' if len(insp[nick]) != 1 else '', nick))
        with open('.inspirational_quotes', 'a') as f:
            for quote in insp[nick]:
                msg.say_back('APPROVED: {}'.format(quote))
                inspiration.append(quote)
                inspiration_use.append(quote)
                f.write(quote + '\n')
            del insp[nick]
    else:
        if nick not in insp:
            msg.say_back('{} is not awaiting approval for any quotes'.format(nick))
        try:
            msg.say_back('APPROVED: {}'.format(insp[nick][ind]))
            inspiration.append(insp[nick][ind])
            inspiration_use.append(insp[nick][ind])
            with open('.inspirational_quotes', 'a') as f:
                f.write(insp[nick][ind] + '\n')
            del insp[nick][ind]
        except IndexError:
            msg.say_back('{} doesn\'t have quote at index {}'.format(nick, ind))

    with open('inspirational_quotes', 'w') as f:
        jdump(insp, f)

approve_insp.message_command = '.approveinsp'
approve_insp.command_equals = 'PRIVMSG'

def rm_insp(msg):
    if not msg.is_owner_host():
        return

    splmsg = msg._message.split()[1:]

    nick = None
    ind = None
    if len(splmsg) > 0:
        nick = splmsg[0]
        if len(splmsg) > 1:
            try:
                ind = int(splmsg[1])
            except ValueError:
                pass

    if not isfile('.inspirational_quotes_add.json'):
        insp = {}
    else:
        with open('.inspirational_quotes_add.json') as f:
            insp = jload(f)

    if nick is None:
        msg.say_back('must specify a nick [and optional index] to remove')
        return

    if ind is None:
        if nick not in insp:
            msg.say_back('{} is not awaiting approval for any quotes'.format(nick))
            return
        msg.say_back('REMOVED {} quotes from {}'.format(len(insp[nick]), nick))
        del insp[nick]
    else:
        if nick not in insp:
            msg.say_back('{} is not awaiting approval for any quotes'.format(nick))
        try:
            msg.say_back('REMOVED: {}'.format(insp[nick][ind]))
            del insp[nick][ind]
        except IndexError:
            msg.say_back('{} doesn\'t have quote at index {}'.format(nick, ind))

    with open('inspirational_quotes', 'w') as f:
        jdump(insp, f)

rm_insp.message_command = '.rminsp'
rm_insp.command_equals = 'PRIVMSG'


def foo(msg):
    msg.say_back('bar')

foo.message_command = 'foo'
foo.command_equals = 'PRIVMSG'


def harglebargle(msg):
    msg.say_back('hargle bargle!')

harglebargle.message_regex = r'\.?[Hh]*[Aa]+[Rr]+[Gg]+[Ll]+[Ee]*\s*[Bb]+[Aa]+[Rr]+[Gg]+[Ll]+[Ee]*[\.\!\?]*'
harglebargle.command_equals = 'PRIVMSG'


def roulette(msg):
    server = msg.get_server()
    channel = msg.get_channel()
    if random() > 5/6:
        server.say(channel, '*BANG*')
        server.raw('KICK {} {} :bang'.format(channel, msg._nick))
    else:
        server.say(channel, '*CLICK*')

roulette.message_command = '.roulette'
roulette.command_equals = 'PRIVMSG'


def shoot(msg):
    splmsg = msg._message.split()
    if len(splmsg) == 1:
        return
    nick = splmsg[1]
    server = msg.get_server()
    channel = msg.get_channel()
    if random() > 1/1000:
        if random() < .5:
            server.say(channel, '*CLICK* Darn thing is jammed again')
        else:
            server.me(channel, 'shoots but has terrible aim')
    else:
        server.say(channel, '*BANG*')
        server.raw('KICK {} {} :bang'.format(channel, nick))

shoot.message_command = '.shoot'
shoot.command_equals = 'PRIVMSG'


def time_in(msg):
    city = msg._message.split(None, 1)[1].lower()
    if city in cities:
        for tzi in cities[city]:
            msg.say_back('{} ({}, {}): {}'.format(city, timezones[tzi][1], timezones[tzi][2], datetime.now(timezone(timezones[tzi][0])).strftime('%a, %b %d, %Y, %H:%M:%S')))
    else:
        msg.say_back('I didn\'t find that city')

time_in.message_command = '.timein'
time_in.command_equals = 'PRIVMSG'


def lmgtfy(msg):
    search = msg._message.split()[1:]
    if not len(search):
        return
    msg.say_back('here; let me google that for you ... http://lmgtfy.com/?q={}'.format(quote_plus(' '.join(search))))

lmgtfy.message_command = '.lmgtfy'
lmgtfy.command_equals = 'PRIVMSG'


def lmddgtfy(msg):
    search = msg._message.split()[1:]
    if not len(search):
        return
    msg.say_back('here; let me duckduckgo that for you ... http://lmddgtfy.net/?q={}'.format(quote_plus(' '.join(search))))

lmddgtfy.message_command = '.lmddgtfy'
lmddgtfy.command_equals = 'PRIVMSG'

def ddg(msg):
    #         query                                             safe search                    nohtml          meanings
    params = {'q': msg._message.split(None, 1)[1], 'o': 'json', 'kp': '1', 'no_redirect': '1', 'no_html': '0', 'd': '0'}
    encparams = urlencode(params)
    url = 'http://api.duckduckgo.com/?' + encparams
    request = Request(url, headers={'User-Agent': 'duckduckgo'})
    try:
        response = urlopen(request)
    except URLError as e:
        print(e)
        return
    try:
        json = jloads(response.read().decode('utf-8', errors='ignore'))
    except Exception as e:
        print(e)
        return
    response.close()
    message = ''
    url = json['AbstractURL']
    meaning = json['Definition']
    if json['AbstractURL']:
        message = json['AbstractURL']
    if meaning:
        message += ' (def: {})'.format(meaning)
    if message == '':
        message = 'I didn\'t find any results'
    msg.say_back(message)

ddg.message_command = '.ddg'


def bot_eval(msg):
    if not msg.is_owner_host():
        return

    try:
        try:
            ret = eval(msg._message.split(' ', 1)[1])
        except:
            ret = exec(msg._message.split(' ', 1)[1])
    except Exception as e:
        ret = 'error: {}'.format(e)
    finally:
        msg.say_back('ret: {}'.format(repr(ret)))

bot_eval.message_command = '!testeval'
bot_eval.command_equals = 'PRIVMSG'

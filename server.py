# -*- coding: utf-8 -*-

from codecs import decode as cdecode
from os.path import isfile
from queue import Queue
from random import randint
from socket import socket, gaierror
from ssl import wrap_socket
from threading import Thread

from logger import Logger
from irc_message import IrcMessage

class Server(object):

    """irc server"""

    # keys used if not passed in kwargs
    """defaults for each new server [dict(str:str)]"""
    _defaults = {
        'name': 'default',
        'nickserv_nick': 'core2_duo',
        'nick': 'core2_duo',
        'user': 'core2_duo',
        'realname': 'core2 duo',
        'password_file': 'freenode_password',
        'host': 'chat.freenode.net',
        'port': 6697,
        'ssl': True,
        'channels': ['##core2'],
        'onjoin': {'##core2': '/me stumbles back in'},
        'autoreconnect': True,
        'max_reconnect': 3,
        'owner_nick': '.*',
        'owner_user': '.*',
        'owner_host': '.*',
    }

    # should be overwritten with server._logger = .. before init
    """logger [logger.Logger]"""
    _logger = None

    def __init__(self, **kwargs):
        """init Server"""

        # check that logger is overwritten
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if self._logger is None:
            self._logger = Logger(out_file='server.log')

        # server specific
        # ~~~~~~~~~~~~~~~
        """host address [str]"""
        self._host = kwargs.get('host', self._defaults['host'])

        """host port [str]"""
        self._port = kwargs.get('port', self._defaults['port'])

        """use ssl [bool]"""
        self._ssl = kwargs.get('nick', self._defaults['ssl'])

        """name of server [str]"""
        self._name = kwargs.get('name', self._defaults['name'])

        """autoreconnect after unintentional disconnection [bool]"""
        self._autoreconnect = kwargs.get('autoreconnect', self._defaults['autoreconnect'])

        """max number of reconnects [int]"""
        self._max_reconnect = kwargs.get('max_reconnect', self._defaults['max_reconnect'])


        # bot specific
        # ~~~~~~~~~~~~
        """bot's nick to auth with nickserv [str]"""
        self._nickserv_nick = kwargs.get('nickserv_nick', self._defaults['nickserv_nick'])

        """bot's nick [str]"""
        self._nick = kwargs.get('nick', self._defaults['nick'])

        """bot's user (usually same as nick) [str]"""
        self._user = kwargs.get('user', self._defaults['user'])

        """bot's realname [str]"""
        self._realname = kwargs.get('realname', self._defaults['realname'])

        """path to password file [str]"""
        self._password_file = kwargs.get('password_file', self._defaults['password_file'])

        """channels to start join after connected [list(str)]"""
        self._start_channels = kwargs.get('channels', self._defaults['channels'])

        """things to say when joining a channel [dict(str:str)]"""
        self._onjoin = kwargs.get('onjoin', self._defaults['onjoin'])

        """owner nick [str]"""
        self._owner_nick = kwargs.get('owner_nick', self._defaults['owner_nick'])

        """owner user [str]"""
        self._owner_user = kwargs.get('owner_user', self._defaults['owner_user'])

        """owner host [str]"""
        self._owner_host = kwargs.get('owner_host', self._defaults['owner_host'])


        # probably not defined
        # ~~~~~~~~~~~~~~~~~~~~
        """irc socket [socket.socket]"""
        self._socket = kwargs.get('socket', None)

        """connection attempts [int]"""
        self._connect_attempts = kwargs.get('connect_attempts', 0)

        """_isupport [dict(str:?)]"""
        self._isupport = kwargs.get('_isupport', {'TARGMAX': 'NAMES:1,LIST:1,KICK:1,WHOIS:1,PRIVMSG:1,NOTICE:1'})

        """channels currently in [dict(str:?)]"""
        self._channels = kwargs.get('channels_joined', {})

        """mode of bot [str]"""
        self._mode = kwargs.get('mode', '')

        """indicates if has identified [bool]"""
        self._identified = kwargs.get('identified', False)

        """hostname of server [str]"""
        self._hostname = kwargs.get('hostname', None)

        """callback once server disconnects"""
        self._callback_on_disconnection = kwargs.get('callback_on_disconnetion', None)

        """queue for messages [queue.Queue]"""
        self._queue = kwargs.get('queue', Queue())

        """indicates if connection is open [bool]"""
        self._has_connection = kwargs.get('has_connection', False)


        # post variable init
        # ~~~~~~~~~~~~~~~~~~
        # protect nick/user/host from being user by someone with nick that starts with same thing as owner
        if self._owner_nick != '.*' and not self._owner_nick.endswith('$'):
            self._owner_nick += '$'
        if self._owner_user != '.*' and not self._owner_user.endswith('$'):
            self._owner_user += '$'
        if self._owner_host != '.*' and not self._owner_host.endswith('$'):
            self._owner_host += '$'

        # convert /me to actual action
        for key,onjoin in self._onjoin.items():
            if onjoin.startswith('/me '):
                self._onjoin[key] = '\x01ACTION{}\x01'.format(onjoin[3:])

    def connect(self):
        """start server and validate that it is connected

        :returns: [None]

        """

        def _check_hostname_(self):
            """(in self.connect()) connect::_check_hostname - just checks to see if hostname was found

            :returns: [None]

            """
            while self._has_connection:
                line = self._queue.get()
                spline = line.split()
                if spline[0] == 'PING':
                    self.raw('PONG {}'.format(spline[1]))
                    continue
                self._logger.log(line, to_file=False)
                if len(line) < 4:
                    continue
                if spline[2].lstrip('0') == '1':
                    pass
                elif  'found your hostname' in line.lower():
                    self._hostname = spline[0].lstrip(':')
                    self._logger.log('found hostname {}'.format(self._hostname), 'good', to_file=False)
                    return

        def _check_connected_(self):
            """(in self.connect()) checks to see if we are connected and also stores valuable info from server

                returns: [None]

            """
            while self._has_connection:
                line = self._queue.get()
                spline = line.split()
                if spline[0] == 'PING':
                    self.raw('PONG {}'.format(spline[1]))
                    continue
                self._logger.log(line, to_file=False)
                if len(spline) < 4:
                    return
                if spline[1] == 'MODE':
                    self._mode = spline[3].lstrip(':')
                    self._logger.log('conneted to {} as {} with mode {}'.format(self._hostname, self._nick, self._mode), 'good')
                    return
                elif spline[1] == '432':
                    # erroneous nick
                    self._logger.log('{} erroneous nick'.format(self._nick), 'warning')
                    self._nick = 'core2_{}'.format(randint(1000, 999999))
                    self._logger.log('new nick: {}'.format(self._nick), 'important')
                    self.raw('NICK {}'.format(self._nick))
                elif spline[1] == '433':
                    # nick taken
                    self._logger.log('{} already taken - will regain'.format(self._nick), 'warning')
                    self._new_nick = '{}_{}'.format(self._nick, randint(1000, 999999))
                    self._logger.log('temp new nick: {}'.format(self._new_nick), 'important')
                    self.raw('NICK {}'.format(self._new_nick))
                elif spline[1] == '437':
                    # nick temporarily unavailable
                    self._logger.log('{} temporarily unavailable - will regain'.format(self._nick), 'warning')
                    self._new_nick = '{}_{}'.format(self._nick, randint(1000, 999999))
                    self._logger.log('temp new nick: {}'.format(self._new_nick), 'important')
                    self.raw('NICK {}'.format(self._new_nick))
                elif spline[1].lstrip('0') == '1':
                    if spline[-1] != self._nick:
                        self._new_nick = spline[-1]
                        self._logger.log('got a new nick: {}'.format(self._new_nick), 'warning')
                elif spline[1].lstrip('0') == '2':
                    new_host = spline[5].split('[')[0]
                elif spline[1].lstrip('0') == '3':
                    self._server_created = line.split(' ', 6)[6]
                elif spline[1].lstrip('0') == '4':
                    self._connected = True
                elif spline[1].lstrip('0') == '5':
                    info = spline[3:]
                    if line.lower().endswith(':are supported by this server'):
                        info = info[:-5]
                    for key in info:
                        if '=' in key:
                            key = key.split('=')
                            val = key[1]
                            key = key[0]
                            self._isupport[key] = val
                        else:
                            self._isupport[key] = None

        def _send_pass_(self, password):
            """(in self._connect()) send password to nickserv and verify

                :password: nickserv password [str]
                :returns: [None]

            """
            if self._new_nick:
                if self._nickserv_nick:
                    self.say('NickServ', 'identify {} {}'.format(self._nickserv_nick, password))
                else:
                    self.say('NickServ', 'identify {} {}'.format(self._nick, password))
            elif self._nickserv_nick:
                self.say('NickServ', 'identify {} {}'.format(self._nickserv_nick, password))
            else:
                self.say('NickServ', 'identify {}'.format(password))
            while self._has_connection:
                line = self._queue.get()
                spline = line.split()
                if len(spline) == 0:
                    continue
                if spline[0] == 'PING':
                    self.raw('PONG {}'.format(spline[1]))
                    continue
                self._logger.log(line, to_file=False)
                strspline = tuple(word.strip(':.,!?*') for word in spline)
                if spline[1] == '396':
                    self._logger.log('Identified as {}'.format(self._nick), 'good')
                    self._identified = True
                    return

        self._connect_attempts += 1

        # create socket
        # ~~~~~~~~~~~~~
        if self._socket is None:
            if self._ssl:
                # create ssl socket
                self._ircsocket = wrap_socket(socket())
            else:
                # create socket
                self._ircsocket = socket()
        elif self._has_connection:
            return

        # reset things not yet defined by server
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self._identified = False
        self._mode = ''
        self._hostname = None
        self._new_nick = None

        try:
            self._ircsocket.connect((self._host, self._port))
            self._logger.log('connected to {} on port {}'.format(self._host, self._port), 'good', to_file=False)
        except gaierror as e:
            self._logger.log('Could not connect to {} on port {}'.format(self._host, self._port), 'error', to_file=False)
            raise RuntimeError(e)

        # create queue for messages
        # ~~~~~~~~~~~~~~~~~~~~~~~~~
        self._has_connection = True
        Thread(target=self._get_new_lines_).start()

        # send nick/user
        # ~~~~~~~~~~~~~~
        self.raw('NICK {}'.format(self._nick))
        self.raw('USER {} 0 * :{}'.format(self._user, self._realname))

        # connecting
        # ~~~~~~~~~~
        # check that we are initially connected to host
        t = Thread(target=_check_hostname_(self))
        t.start()
        t.join(timeout=10)

        if not self._hostname:
            raise RuntimeError('Could not connect to {}'.format(self._hostname))

        # make sure that we have run through isupport, motd, etc
        t = Thread(target=_check_connected_(self))
        t.start()
        t.join(timeout=10)

        if not self._mode:
            raise RuntimeError('Could not properly connect to {}'.format(self._hostname))

        # parse ISupport
        # ~~~~~~~~~~~~~~
        self._isupport['CPRIVMSG'] = 'CPRIVMSG' in self._isupport
        self._isupport['CNOTICE'] = 'CNOTICE' in self._isupport
        self._isupport['KNOCK'] = 'KNOCK' in self._isupport

        targmax = dict(targ.split(':') for targ in self._isupport['TARGMAX'].split(','))
        self._isupport['TARGMAX'] = {}

        # max number of privmsgs one can send at once
        self._isupport['TARGMAX']['PRIVMSG'] = int(targmax['PRIVMSG'])

        # max number of notices one can send at once
        self._isupport['TARGMAX']['NOTICE'] = int(targmax['NOTICE'])

        if 'CHANTYPES' in self._isupport:
            self._isupport['CHANTYPES'] = tuple(self._isupport['CHANTYPES'])

        # convert from channel prefix to chanserv flag
        if 'PREFIX' in self._isupport:
            self._isupport['PREFIX'] = self._isupport['PREFIX'][1:].split(')') # tmp
            self._isupport['PREFIX'] = dict(zip(self._isupport['PREFIX'][1], self._isupport['PREFIX'][0])) # e.g. {'+': 'v', '@': 'o'}

        if 'STATUSMSG' in self._isupport:
            self._isupport['STATUSMSG'] = tuple(self._isupport['STATUSMSG'])

        # max number of chars in a nick
        if 'NICKLEN' in self._isupport:
            self._isupport['NICKLEN'] = int(self._isupport['NICKLEN'])

        # max number of modes per mode command
        if 'MODES' in self._isupport:
            self._isupport['MODES'] = int(self._isupport['MODES'])

        # max topic length
        if 'TOPICLEN' in self._isupport:
            self._isupport['TOPICLEN'] = int(self._isupport['TOPICLEN'])

        # max length for kick comment
        if 'KICKLEN' in self._isupport:
            self._isupport['KICKLEN'] = int(self._isupport['KICKLEN'])

        # max length for away message
        if 'AWAYLEN' in self._isupport:
            self._isupport['AWAYLEN'] = int(self._isupport['AWAYLEN'])

        # max number of bans a channel can have
        if 'MAXLIST' in self._isupport:
            self._isupport['MAXLIST'] = dict(t.split(':') for t in self._isupport['MAXLIST'].split(','))
            for key,val in self._isupport['MAXLIST'].items():
                self._isupport['MAXLIST'][key] = int(val)

        # max number of channels you can be in at once (per each type of channel listed in CHANTYPES)
        if 'CHANLIMIT' in self._isupport:
            self._isupport['CHANLIMIT'] = dict(chan.split(':') for chan in self._isupport['CHANLIMIT'].split(','))
            for key,val in self._isupport['CHANLIMIT'].items():
                self._isupport['CHANLIMIT'][key] = int(val)

        # read password
        # ~~~~~~~~~~~~~
        if isfile(self._password_file):
            with open(self._password_file, 'rb') as f:
                password = cdecode(cdecode(f.read(), 'base64').decode('ascii'), 'rot13')
        else:
            password = None

        # send password if necessary
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~
        if password:
            t = Thread(target=_send_pass_, args=(self, password))
            t.start()
            t.join(timeout=10)
            if not self._identified:
                self._logger.log('could not identify with nickserv', 'warning')

        if self._new_nick and password:
            # nick is in use
            self._logger.log('{} is in use. ghosting'.format(self._nick), 'warning')
            # self.say('NickServ', 'identify {} {}'.format(self._nick, password))
            self.say('NickServ', 'regain {} {}'.format(self._nick, password))
            self.say('NickServ', 'ghost {} {}'.format(self._nick, password))
            self.raw('NICK {}'.format(self._nick))

        # idk don't need this anymore
        del self._new_nick

        # join channels
        # ~~~~~~~~~~~~~
        for channel in self._start_channels:
            self._logger.log('Joining {}'.format(channel), 'normal', to_file=False)
            self.join(channel)

    def mainloop(self, queue):
        """mainloop for server

        :queue: queue to put all messages in [queue.Queue]
        :returns: [None]

        """
        while self._has_connection:
            line = self._queue.get()
            spline = line.split()
            # handle ping internally
            if spline[0] == 'PING':
                self.raw('PONG {}'.format(spline[1]))
            self._logger.log(line, to_stdout=False)
            # place msg then *args into queue
            queue.put(IrcMessage(line, self._name))

    def raw(self, msg):
        """send raw message to the irc server

            :msg: raw message to send to the server [str]
            :returns: [None]

        """
        self._ircsocket.send('{}\r\n'.format(msg).encode('utf-8'))

    def say(self, to, msg):
        """send a PRIVMSG to either nick or chan

            :to: nick/chan to send the message to [str]
            :msg: message to send [str]
            :returns: [None]

        """
        to = to.split(',')
        tos = []
        for i in range(0, len(to), self._isupport['TARGMAX'].get('PRIVMSG', 1)):
            tos.append(','.join(to[i:i + self._isupport['TARGMAX'].get('PRIVMSG', 1)]))
        for to in tos:
            self.raw('PRIVMSG {} :{}'.format(to, msg))

    def me(self, to, msg): # {{{
        """send /me action to either nick or chan

            :to: nick/chan to send the message to [str]
            :msg: message to send [str]
            :returns: [None]

        """
        to = to.split(',')
        tos = []
        for i in range(0, len(to), self._isupport['TARGMAX'].get('PRIVMSG', 1)):
            tos.append(','.join(to[i:i + self._isupport['TARGMAX'].get('PRIVMSG', 1)]))
        for to in tos:
            self.say(to, '\x01ACTION {}\x01'.format(msg))

    def notice(self, to, msg):
        """send a NOTICE to either nick or chan

            :to: nick/chan to send the message to [str]
            :msg: message to send [str]
            :returns: [None]

        """
        to = to.split(',')
        tos = []
        for i in range(0, len(to), self._isupport['TARGMAX'].get('PRIVMSG', 1)):
            tos.append(','.join(to[i:i + self._isupport['TARGMAX'].get('PRIVMSG', 1)]))
        for to in tos:
            self.raw('NOTICE {} :{}'.format(to, msg))

    def mode(self, chan, modes, objs):
        """set modes for nicks/chans

            :chan: channel to change modes within/on [str]
            :modes: modes to use [str]
            :objs: nicks/chans to change modes on [str]
            :returns: [None]

        """
        # current type (either + or -)
        cur_type = modes[0]
        last_type = None

        # flags must start with + or -
        if not (cur_type == '+' or cur_type == '-'):
            return False

        objs = objs.split()

        # number of modes
        mode_count = 0
        # number of objs
        obj_count = len(objs)

        new_modes = []
        for i in modes:
            if i == '+' or i == '-':
                cur_type = i
                continue
            if mode_count == obj_count:                         # too many flags
                break
            if not mode_count % self._isupport.get('MODES', 1): # self.max_modes modes have been added (or 0, so it's at begginning)
                new_modes.append('')                            # create new set
                last_type = None                                # this lets type be specified for new set of 4
            if cur_type != last_type:                           # we need to add the type (+/-) because it's not yet been specified
                new_modes[-1] += cur_type                       # since type hasn't been specified, add type
                last_type = cur_type                            # set last_type so we don't duplicate
            new_modes[-1] += i                                  # add the mode
            mode_count += 1                                     # increase mode count so we know when to do new_modes.append('') (and to keep track of # modes)

        if obj_count > mode_count:
            objs = objs[:mode_count]

        new_objs = []
        for i in range(0, len(objs), self._isupport.get('MODES', 1)):
            new_objs.append(' '.join(objs[i:i + self._isupport.get('MODES')]))

        for modes, objs in zip(new_modes, new_objs):
            self.raw('MODE {} {} {}'.format(chan, modes, objs))

    def join(self, channels, skip_add=False):
        """join a channel

            :chan: channel to join [str]
            :skip_add: do not add channel to self.channels [bool] (default: False)
            :returns: [None]

        """
        self.raw('JOIN {}'.format(channels))
        for chan in channels.split(','):
            chan = chan.split()[0]
            if chan in self._onjoin:
                self.say(chan, self._onjoin[chan])

    def part(self, chan, msg = 'bye'):
        """part a channel

            :chan: channel to part [str]
            :msg: message to send when parting [str] (default: 'bye')
            :returns: [None]

        """
        if chan in self._channels:
            del self._channels[chan]
        self.raw('PART {} :{}'.format(chan, msg))

    def _get_new_lines_(self):
        """read bytes from socket, yield full line

            :returns: [None]

        """
        end_msg = b'\r\n'
        decode_type = 'backslashreplace'

        if not hasattr(self, '_rbytes'):
            self._rbytes = b''

        while self._has_connection:
            # read bytes
            read = self._ircsocket.recv(1024)

            if not read:
                # socket closed
                self._has_connection = False
                if self._callback_on_disconnection is not None:
                    self._callback_on_disconnection(self._name)
                return

            # messages split on \r\n
            rlines = (self._rbytes + read).split(end_msg)
            # pop after \r\n
            self._rbytes = rlines.pop()
            # all decoded full lines
            rlines = [line.decode('utf-8', decode_type) for line in rlines]

            for line in rlines:
                if line:
                    self._queue.put(line)

    def __repr__(self):
        return '{}/{}'.format(self._name, self._host, self._port)

    def __str__(self):
        extras = dict( nick = self._nick, realname = self._user, channels = self._channels, onjoin = self._onjoin, owner_nick = self._owner_nick, owner_user = self._owner_user, owner_host = self._owner_host, )
        return '{}: {}/{} ({})'.format(self._name, self._host, self._port, extras)

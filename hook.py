# -*- coding: utf-8 -*-

from copy import copy
from re import compile as recompile, match as rematch
from threading import Thread

from logger import Logger

class Hook(object):

    """Hook"""

    _handled_matches = {
        # irc_message._raw
        'raw_equals':         str,
        'raw_regex':          str,

        # irc_message._command
        'command_equals':     str,
        'command_regex':      str,
        'command_tuple':      tuple,

        # irc_message._message
        'message_equals':     str,
        'message_regex':      str,

        # irc_message._prefix
        'prefix_equals':      str,
        'prefix_regex':       str,
        'prefix_tuple':       tuple,

        # irc_message._nick
        'nick_equals':        str,
        'nick_regex':         str,
        'nick_tuple':         tuple,

        # irc_message._user
        'user_equals':        str,
        'user_regex':         str,
        'user_tuple':         tuple,

        # irc_message._host
        'host_equals':        str,
        'host_regex':         str,
        'host_tuple':         tuple,

        # irc_message._server_name
        'server_name_equals': str,
        'server_name_tuple':  tuple,

        # irc_message.is_owner{nick,user,host}() to check
        'owner_nick_flag':    bool,
        'owner_user_flag':    bool,
        'owner_host_flag':    bool,

        # special matches
        'message_command':    str,
        'message_len_min':    int,
        'message_len_max':    int,
    }

    _priorities = ('high', 'medium', 'low')

    # should be overwritten with Hook._logger = .. before init
    """logger [logger.Logger]"""
    _logger = None

    def __init__(self, name, callback):
        """TODO: to be defined1.

        :name: name of Hook
        :callback: callback function on successful process

        """

        # check that logger is overwritten
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if self._logger is None:
            self._logger = Logger(out_file='hook.log')

        """name of Hook"""
        self._name = name

        """callback function when match successful"""
        self._callback = callback

        self._thread_callback = bool(getattr(callback, 'thread', True))

        # TODO
        self._priority = getattr(callback, 'priority', 'low')
        if self._priority not in self._priorities:
            self._priority = self._priorities[-1]

        """all attributes that are not private and aren't handled by this hook"""
        self._other_atts = {}

        # matches
        # ~~~~~~~
        """all matches"""
        self._equals_matches = {}

        """regex matches"""
        self._regex_matches = {}

        """list matches"""
        self._tuple_matches = {}

        """owner matches"""
        self._owner_matches = {}

        """special matches"""
        self._special_matches = {}

        for att_name in dir(self._callback):
            if att_name.startswith('__'):
                continue

            match = getattr(self._callback, att_name)

            if att_name not in self._handled_matches:
                if att_name != 'thread':
                    self._other_atts[att_name] = match
                continue

            handled_match_type = self._handled_matches[att_name]

            if type(match) is not handled_match_type:
                continue

            if handled_match_type is tuple:
                if att_name.endswith('_tuple'):
                    cont = False
                    for o in match:
                        if type(o) is not str:
                            self._logger.log('all elements in tuple match must be `str` - not using {}'.format(att_name), 'warning')
                            cont = True
                            break
                    if cont:
                        continue
                    self._tuple_matches[att_name.split('_', 1)[0]] = match
            elif att_name.endswith('_equals'):
                self._equals_matches[att_name.split('_', 1)[0]] = match
            elif att_name.endswith('_regex'):
                self._regex_matches[att_name.split('_', 1)[0]] = recompile(match)
            elif att_name.startswith('owner_') and att_name.endswith('_flag'):
                self._owner_matches[att_name.split('_', 2)[1]] = match
            else:
                # special match
                self._special_matches[att_name] = match

    def process(self, msg):
        for match_name, match in self._equals_matches.items():
            if match != getattr(msg, '_' + match_name):
                return False
        for match_name, match in self._regex_matches.items():
            m = rematch(match, getattr(msg, '_' + match_name))
            if not m:
                return False
            msg.regex_groups = m.groups()
        for match_name, matches in self._tuple_matches.items():
            msg_att = getattr(msg, '_' + match_name)
            if msg_att not in matches:
                return False
        for match_name, match in self._owner_matches.items():
            if not getattr(msg, 'is_owner_' + match_name)():
                return False

        message_command = self._special_matches.get('message_command')
        message_len_min = self._special_matches.get('message_len_min')
        message_len_max = self._special_matches.get('message_len_max')

        if msg._message is None:
            split_message = None
        else:
            split_message = msg._message.split()

        if message_command is not None and (split_message is None or split_message[0] != message_command):
            return False
        if message_len_min is not None and (split_message is None or len(split_message) < message_len_min):
            return False
        if message_len_max is not None and (split_message is None or len(split_message) > message_len_max):
            return False

        if self._thread_callback:
            Thread(target=self._callback_wrapper_, args=(msg,)).start()
        else:
            self._callback_wrapper_(msg)

        return True

    def _callback_wrapper_(self, msg):
        try:
            self._callback(copy(msg))
        except Exception as e:
            self._logger.log('error in Hook ({}) function: {}'.format(self._name, e), 'error')
            msg.say_back('error in Hook ({}) function: {}'.format(self._name, e))

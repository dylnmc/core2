# -*- coding: utf-8 -*-

from importlib.util import module_from_spec, spec_from_file_location
from filecmp import cmp as same_file
from re import compile as recompile, match as rematch
from os import listdir
from os.path import abspath, isfile, join as osjoin, expanduser, expandvars
from shutil import copy as cp
from threading import Thread
from types import FunctionType

from hook import Hook
from logger import Logger

class EventHandler(object):

    """Event Handler"""

    # should be overwritten with EventHandler._logger = .. before init
    """logger [logger.Logger]"""
    _logger = None

    def __init__(self, module_dirs):
        """init Event Handler"""

        # check that logger is overwritten
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if self._logger is None:
            self._logger = Logger(out_file='event_handler.log')

        # overwrite logger
        # ~~~~~~~~~~~~~~~~
        Hook._logger = self._logger

        """directories that contain modules [list(str)]"""
        self._module_dirs = module_dirs

        """hooks that are added from modules [dict(str:hook.Hook)]"""
        self._hooks = {}

        """dict of imported modules [dict(str:module]"""
        self._modules = []

        # load hooks
        self.load_hooks()

    def process(self, msg):
        """process deconstructed msg, check matches, call callback

        :msg: deconstructed msg [dict(str:str)]
        :returns: None

        """
        Thread(target=self._process_wrapper_, args=(msg,)).start()

    def _process_wrapper_(self, msg):
        """wrapper to process messages in thread

        :msg: deconstructed msg [dict(str:str)]
        :returns: None

        """
        for hook in self._hooks.values():
            hook.process(msg)

    def load_hooks(self):
        """load hooks from module dirs

        :returns: errors [list(Error)]

        """
        new_modules = {}
        new_module_paths = []
        errors = []

        for module_dir in self._module_dirs:
            module_full_dir = abspath(expanduser(expandvars(module_dir)))

            # duplicate entry
            if module_full_dir in new_module_paths:
                continue
            new_module_paths.append(module_full_dir)

            for f in listdir(module_full_dir):
                module_file = osjoin(module_full_dir, f)
                self.load_file(module_file, new_modules, errors)

        self._hooks = {}
        self._modules = new_modules

        for module_name, module in self._modules.items():
            self.load_module(module_name, module, new_modules)

        return errors

    def load_file(self, file_name, new_modules={}, errors=[]):
        """loads file into new_modules

        :file_name: name of python file that is hopefully a valid module [str]
        :new_modules: new_modules to potentially add to [dict(str:module)]
        :errors: list of errors [list(Exception)]
        :returns: loaded file succesfully into new_modules? [bool]

        """
        # should be a python file
        if not (isfile(file_name) and file_name.endswith('.py')):
            return False

        # unique module name
        module_name = file_name.rsplit('/', 1)[1][:-3]
        if module_name in new_modules:
            module_name = file_name[:-3]

        try:
            spec = spec_from_file_location(module_name, file_name)
            module = module_from_spec(spec)
            spec.loader.exec_module(module)
        except Exception as e:
            errors.append(e)
            if module_name in self._modules:
                module = self._modules[module_name]
                self._logger.log('loaded backup module {} - error: {}'.format(module_name, e), 'error')
            else:
                self._logger.log('could not load {} - error: {}'.format(module_name, e), 'error')
                return

        # __no_load__ is true - don't load module
        if hasattr(module, '__no_load__') and module.__no_load__:
            return False

        new_modules[module_name] = module

        return True

    def load_module(self, module_name, module, new_modules={}):
        """loads module

        :module_name: unique name for module [str]
        :module: module [module]
        :new_modules: place to put successfully loaded module [dict(str:module)]
        :returns: None

        """

        # call __setup__ if module has setup function
        if hasattr(module, '__setup__'):
            module.__setup__()

        for module_attr in dir(module):
            # private variable
            if module_attr.startswith('__'):
                continue

            function = getattr(module, module_attr)

            self.load_hook(module_name, function)

    def load_hook(self, module_name, function):
        """loads a hook

        :function: function to load as a hook
        :returns: successfully loaded hook? [bool]

        """
        if (not isinstance(function, FunctionType)                          # not a function
            or (hasattr(function, '__no_load__') and function.__no_load__)  # __no_load__ is True
            or function.__module__ != module_name):                         # import
            return False

        hook = Hook('{}*{}'.format(module_name, function.__name__), function)

        # this hook doesn't have anything to match
        if (len(hook._equals_matches) == 0 and len(hook._regex_matches) == 0 and
            len(hook._tuple_matches) == 0 and (hook._special_matches) == 0):
            return False

        self._hooks[hook._name] = hook

        # self._logger.log('Hook ({}) successfully loaded'.format(hook._name), 'good', to_stdout=False)
        self._logger.log('Hook ({}) successfully loaded'.format(hook._name), 'good')

        return True

    def rm_hook(self, hook_name):
        if hook_name in self._hooks:
            del self._hooks[hook_name]
            return True
        return False
